import {
    FETCH_PHOTO_SUCCESS,
    FETCH_FAILURE,
} from "../actionTypes";

const initialState = {
    photo: null,
    error: null
};

const placesReducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_PHOTO_SUCCESS:
            return {...state, photo: action.photo};
        case FETCH_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default placesReducer;
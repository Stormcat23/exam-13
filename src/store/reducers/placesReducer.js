import {
  FETCH_PLACES_SUCCESS,
  FETCH_PLACE_SUCCESS,
  FETCH_FAILURE,
} from "../actionTypes";

const initialState = {
  places: null,
  place: null,
  error: null
};

const placesReducer = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_PLACES_SUCCESS:
      return {...state, places: action.places};
    case FETCH_PLACE_SUCCESS:
      return {...state, place: action.place};
    case FETCH_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default placesReducer;
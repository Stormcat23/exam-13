import axios from "../../axiosApi";
import {
    CREATE_PHOTO_SUCCESS,
    FETCH_FAILURE, FETCH_PHOTO_SUCCESS
} from "../actionTypes";


const fetchPhotoSuccess = photo => {
    return {type: FETCH_PHOTO_SUCCESS, photo};
};

const fetchFailure = error => {
    return {type: FETCH_FAILURE, error};
};

const createPhotoSuccess = () => {
    return {type: CREATE_PHOTO_SUCCESS};
};

export const fetchPhoto = (id) => {
    return async dispatch => {
        try {
            await axios.get(`/photo?place=${id}`).then(response => {
                dispatch(fetchPhotoSuccess(response.data));
            })
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchFailure(e.response.data));
            } else {
                dispatch(fetchFailure({global: "No internet"}));
            }
        }
    };
};

export const createPhoto = (photoData, id) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            await axios.post("/photo", photoData, {headers});
            dispatch(createPhotoSuccess());
            console.log(id)
            dispatch(fetchPhoto(id));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchFailure(e.response.data));
            } else {
                dispatch(fetchFailure({global: "No internet"}));
            }
        }
    };
};





import axios from "../../axiosApi";
import {
    CREATE_PLACES_SUCCESS,
    FETCH_PLACES_SUCCESS,
    FETCH_PLACE_SUCCESS,
    FETCH_FAILURE, CREATE_REVIEW_SUCCESS
} from "../actionTypes";
import {push} from "connected-react-router";

const fetchPlacesSuccess = places => {
    return {type: FETCH_PLACES_SUCCESS, places};
};

const fetchPlaceSuccess = place => {
    return {type: FETCH_PLACE_SUCCESS, place};
};

const fetchFailure = error => {
    return {type: FETCH_FAILURE, error};
};

const createPlaceSuccess = () => {
    return {type: CREATE_PLACES_SUCCESS};
};

const createReviewSuccess = () => {
    return { type: CREATE_REVIEW_SUCCESS }
};

export const fetchPlaces = () => {
    return async dispatch => {
        try {
            await axios.get("/places").then(response => {
                dispatch(fetchPlacesSuccess(response.data));
            })
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchFailure(e.response.data));
            } else {
                dispatch(fetchFailure({global: "No internet"}));
            }
        }
    };
};

export const fetchPlace = (id) => {
    return async dispatch => {
        try {
            await axios.get(`/places/${id}`).then(response => {
                dispatch(fetchPlaceSuccess(response.data));
            })
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchFailure(e.response.data));
            } else {
                dispatch(fetchFailure({global: "No internet"}));
            }
        }
    };
};

export const createPlace = placeData => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            await axios.post("/places", placeData, {headers})
            dispatch(createPlaceSuccess());
            dispatch(push("/"));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchFailure(e.response.data));
            } else {
                dispatch(fetchFailure({global: "No internet"}));
            }
        }
    };
};

export const createReview = (reviewData) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            await axios.post("/reviews", reviewData, {headers});
            dispatch(createReviewSuccess());
            dispatch(fetchPlace(reviewData.place));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchFailure(e.response.data));
            } else {
                dispatch(fetchFailure({global: "No internet"}));
            }
        }
    };
};





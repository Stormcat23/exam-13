import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import NewPlace from "./containers/NewPlace/NewPlace";
import Places from "./containers/Places/Places";
import FullPlaceCard from "./components/FullPlaceCard/FullPlaceCard";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ? <Route {...props} /> : <Redirect to={redirectTo}/>
};

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={Places}/>
            <ProtectedRoute
                path="/register"
                exact
                component={Register}
                isAllowed={!user}
                redirectTo="/"
            />
            <ProtectedRoute
                path="/login"
                exact
                component={Login}
                isAllowed={!user}
                redirectTo="/"
            />
            <ProtectedRoute
                path="/add_new_place"
                exact
                component={NewPlace}
                isAllowed={user}
                redirectTo="/login"
            />
            <ProtectedRoute
                path="/places/:id"
                exact
                component={FullPlaceCard}
                isAllowed={user}
                redirectTo="/login"
            />
            <Route render={() => <h1>404 Not Found</h1>}/>
        </Switch>
    );
}

export default Routes;
import React, {useEffect} from "react";
import {Box, Grid, Paper} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchPlace} from "../../store/actions/placesActions";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import {apiURL} from "../../constants";
import AddPhotoForm from "../Form/AddPhotoForm";
import {fetchPhoto} from "../../store/actions/photoActions";
import {makeStyles} from "@material-ui/core/styles";
import Photo from "../Photo/Photo";
import ReviewForm from "../ReviewForm/ReviewForm";
import {Rating} from "@material-ui/lab";

const {nanoid} = require("nanoid");

const useStyles = makeStyles((theme) => ({
    container: {
        borderTop: "3px solid black",
        borderBottom: "3px solid black"
    },
    newDiv: {
        display: "flex",
        flexDirection: "row",
        width: "1200px"
    },
    comments: {
        display: "flex",
        width: "1200px",
        flexWrap: "wrap",
        flexDirection: "row"
    },
    comment: {
        border: "1px solid gold",
        width: '50%',
        display: "flex"
    }
}));


const FullPlaceCard = (props) => {
        const dispatch = useDispatch();
        const classes = useStyles();
        const id = props.match.params.id;
        const place = useSelector(state => state.places.place);
        const photos = useSelector(state => state.photo.photo);
        useEffect(() => {
            dispatch(fetchPlace(id));
            dispatch(fetchPhoto(id));
        }, [dispatch, id]);

        return (
            <Grid container direction="column" spacing={2}>
                {place && place[0] &&
                <Grid item container direction="row" spacing={2} wrap="wrap-reverse">
                    <Grid item xs={12} md={7}>
                        <Paper elevation={3} style={{padding: "5px"}}>
                            <CardMedia
                                component="img"
                                image={apiURL + "/uploads/" + place[0].image}
                            />
                        </Paper>
                    </Grid>
                    <Grid item xs={12} md={5}>
                        <Typography component="h4" variant="h4" style={{textTransform: 'capitalize'}}>
                            {place[0].title}
                        </Typography>
                        <Typography component="p" variant="subtitle1">
                            {place[0].description}
                        </Typography>
                    </Grid>

                    {place && place[0] && <Box component="fieldset" borderColor="transparent">
                        <Typography component="legend">Over All</Typography>
                        <Rating name="read-only" value={place[0].totalScores.overAll} readOnly/>
                        <Typography component="legend" >{place[0].totalScores.overAll ? parseFloat(place[0].totalScores.overAll).toFixed(1) : ''}</Typography>
                    </Box>
                    }

                    {place && place[0] && <Box component="fieldset" borderColor="transparent">
                        <Typography component="legend">Quality Of Food</Typography>
                        <Rating name="read-only" value={place[0].totalScores.qualityOfFood} readOnly/>
                        <Typography component="legend" >{place[0].totalScores.qualityOfFood ? parseFloat(place[0].totalScores.qualityOfFood).toFixed(1) : ''}</Typography>
                    </Box>
                    }

                    {place && place[0] && <Box component="fieldset" borderColor="transparent">
                        <Typography component="legend">Service Quality</Typography>
                        <Rating name="read-only" value={place[0].totalScores.serviceQuality} readOnly/>
                        <Typography component="legend" >{place[0].totalScores.serviceQuality ? parseFloat(place[0].totalScores.serviceQuality).toFixed(1) : ''}</Typography>
                    </Box>
                    }
                    {place && place[0] && <Box component="fieldset" borderColor="transparent">
                        <Typography component="legend">Interior</Typography>
                        <Rating name="read-only" value={place[0].totalScores.interior} readOnly/>
                        <Typography component="legend" >{place[0].totalScores.interior ? parseFloat(place[0].totalScores.interior).toFixed(1) : ''}</Typography>
                    </Box>
                    }
                </Grid>
                }
                <Grid container direction="column" spacing={2}>
                    <Grid item container xs={12} md={5} direction="row">
                        <div className={classes.newDiv}>
                            {photos && photos.map(photo => {
                                return (
                                    photo.image.map(name => {
                                        return <Photo image={apiURL + "/uploads/" + name} key={nanoid(6)}/>
                                    })
                                );
                            })}
                        </div>
                    </Grid>
                </Grid>
                <AddPhotoForm id={id}/>
                <div className={classes.comments}>
                    {place && place[0] && place[0].reviews.map(item => {
                        return (
                        <div className={classes.comment}>
                        <Box component="fieldset" borderColor="transparent">
                            <Typography component="legend">Quality Of Food</Typography>
                            <Rating name="read-only" value={item.qualityOfFood} readOnly/>
                            <Typography component="legend" >{item.qualityOfFood ? parseFloat(item.qualityOfFood).toFixed(1) : ''}</Typography>
                        </Box>

                        <Box component="fieldset" borderColor="transparent">
                            <Typography component="legend">Service Quality</Typography>
                            <Rating name="read-only" value={item.serviceQuality} readOnly/>
                            <Typography component="legend" >{item.serviceQuality ? parseFloat(item.serviceQuality).toFixed(1) : ''}</Typography>
                        </Box>

                        <Box  component="fieldset" borderColor="transparent">
                            <Typography component="legend">Interior</Typography>
                            <Rating name="read-only" value={item.interior} readOnly/>
                            <Typography component="legend" >{item.interior ? parseFloat(item.interior).toFixed(1) : ''}</Typography>

                        </Box>
                            <Typography component="legend" >{item.comment}</Typography>
                        </div>

                        )
                    })}
                </div>
                <h3>Напишите Ваш отзыв: </h3>
                <ReviewForm id={id}/>
            </Grid>

        );
    }
;

export default FullPlaceCard;
import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import PropTypes from "prop-types";
import {apiURL} from "../../constants";
import {NavLink} from "react-router-dom";
import './PlaceCard.css';
import { Link } from "react-router-dom";


const useStyles = makeStyles({
    root: {
        width: 400,
        marginLeft: 15,
        marginBottom: 15,
    },
    title: {
        flexGrow: 1
    }
});

const PlaceCard = ({id, title, image}) => {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardActionArea id={id} component={Link} to={"/places/" + id}>
                <CardMedia
                    component="img"
                    alt="Contemplative Reptile"
                    height="300"
                    image={apiURL + "/uploads/" + image}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2" className={classes.title}>
                        {title}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                {
                    <NavLink exact to={`/places/${id}`} className="button-4" id="learnMorePlace"><span>Learn More</span></NavLink>
                }

            </CardActions>
        </Card>
    );
};

PlaceCard.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string,
    datetime: PropTypes.string,
}

export default PlaceCard;
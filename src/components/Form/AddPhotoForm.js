import React, {useState, useRef} from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import CssBaseline from '@material-ui/core/CssBaseline';
import {createPhoto} from "../../store/actions/photoActions";
import {useDispatch, useSelector} from "react-redux";

const useStyles = makeStyles((theme) => ({
    remove: {
        cursor: "pointer",
        '&:hover': {
            color: "red"
        }
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(1, 0, 1),
    },
    label: {
        width: '100%',
    },
    imagesBlock: {
        display: "flex",
        flexWrap: "wrap",
        width: "98.5%",
        margin: "0 auto"
    },
    image: {
        display: "block",
        width: "150px",
        height: "150px",
        margin: "0 auto",
    },
    img: {
        marginRight: "10px",
        border: "1px solid black",
        width: "152px",
        height: "152px",
        position: "relative"
    },
    delete: {
        position: "absolute",
        top: "0",
        right: "0",
        '&:hover': {
            color: "red"
        }
    },
    container: {
        borderTop: "3px solid black",
        borderBottom: "3px solid black"
    }
}));

export default function AddPhotoForm(props) {
    const classes = useStyles();
    const inputRef = useRef();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const [photoData, setPhotoData] = useState({
        image: [],
        user: user._id,
        place: props.id
    });

    const [images, setImages] = useState({images: []});

    const fileChangeHandler = e => {
        const files = e.target.files;
        if (files) {
            const inputDataCopy = {...photoData};
            const copyImg = inputDataCopy.image;
            for (const key in files) {
                if (typeof files[key] === "object") {
                    copyImg.push(files[key]);
                }
            }
            setPhotoData({
                ...photoData,
                image: copyImg
            });
            if (window.FileList && window.File && window.FileReader) {
                for (const key in files) {
                    let reader = new FileReader();
                    reader.addEventListener('load', (event) => {
                        const imagesCopy = images.images;
                        imagesCopy.push(event.target.result);
                        setImages({
                            ...images,
                            images: imagesCopy,
                        });
                    });
                    if (typeof files[key] === "object") {
                        reader.readAsDataURL(files[key]);
                    }
                }
            }
        }
    };

    const deleteImg = (ind) => {
        const inputDataCopy = {...photoData};
        const copyImg = inputDataCopy.image;
        copyImg.splice(ind, 1);
        setPhotoData({
            ...photoData,
            image: copyImg,
        });
        const imagesCopy = images.images;
        imagesCopy.splice(ind, 1);
        setImages({
            ...images,
            images: imagesCopy,
        });
    };

    const onClickAddPhoto = async e => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('place', photoData.place);
        formData.append('user', photoData.user);
        for (let i = 0; i < photoData.image.length; i++) {
            formData.append(`image`, photoData.image[i])
        }
        dispatch(createPhoto(formData, props.id));
        setImages({
            ...images,
            images: []
        });
    };

    return (
        <Container component="main" className={classes.container}>
            <CssBaseline/>
            <div className={classes.paper}>
                <form className={classes.form} onSubmit={onClickAddPhoto}>
                        <Grid item xs={12}>
                        </Grid>
                        <input
                            accept="image/*"
                            style={{ display: 'none' }}
                            id="raised-button-file"
                            multiple
                            name='image'
                            ref={inputRef}
                            onChange={fileChangeHandler}
                            type="file"
                        />
                        <h2
                            style={{
                                paddingLeft: "10px",
                                marginBottom: "0px",
                            }}
                        >
                            Новые фотографии
                        </h2>
                        <div className={classes.imagesBlock}>
                            {images.images && images.images.map((img, ind) => {
                                return (
                                    <div key={ind}>
                                        {
                                            img &&
                                            <div className={classes.img}>
                                                <HighlightOffIcon
                                                    className={classes.delete}
                                                    value={ind} onClick={() => deleteImg(ind)}
                                                />
                                                <img
                                                    className={classes.image}
                                                    src={img} alt=""
                                                />
                                            </div>
                                        }
                                    </div>
                                );
                            })}
                            {!images.images[0] && <p>Если у вас есть фотографии этого заведения, загрузите их.</p>}
                        </div>
                        <label htmlFor="raised-button-file" className={classes.label}>
                            <Button variant="outlined"
                                    component="span"
                                    id={"add_photo"}
                                    fullWidth
                                    className={classes.submit}
                            >
                                Загрузить фото
                            </Button>
                        </label>
                    <Button
                        id={"add_answer"}
                        type="submit"
                        fullWidth
                        variant="outlined"
                        className={classes.submit}
                    >
                            Отправить фото
                    </Button>
                </form>
            </div>
        </Container>
    );
}


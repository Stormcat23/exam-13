import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import FormElement from "../Form/FormElement";
import FileInput from "../Form/FileInput";
import {useSelector} from "react-redux";
import {Checkbox, FormControlLabel, FormHelperText} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '100%',
        },
    },
}));

const PlaceCreationForm = ({onSubmit}) => {
    const classes = useStyles();
    const error = useSelector(state => state.places.error);
    const [state, setState] = useState({
        title: "",
        description: "",
        image: "",
        agree: "no"
    });

    const submitFormHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setState(prevState => ({...prevState, [name]: file}));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch(e) {
            return undefined;
        }
    };

    return (
        <form
            className={classes.root}
            noValidate
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
            <FormElement
                error={getFieldError("title")}
                name="title"
                label="Title"
                required={true}
                value={state.title}
                onChange={inputChangeHandler}
            />
            <FormElement
                error={getFieldError("description")}
                name="description"
                label="Description"
                required={true}
                value={state.description}
                onChange={inputChangeHandler}
            />
            <FormControl fullWidth className={classes.margin} variant="outlined">
                <FileInput
                    error={getFieldError("image")}
                    required={true}
                    label="Image"
                    name="image"
                    onChange={fileChangeHandler}
                />
            </FormControl>
            <Grid item container xs direction='column'>
                <Grid item container>
                    <FormControlLabel
                        label="I understand"
                        control={<Checkbox error={getFieldError("agree")} color="default" value='yes' required={true} onChange={inputChangeHandler} name="agree" />}
                    />
                </Grid>
                <Grid item>
                    <FormHelperText>By submitting this form, you agree that the following information will be submitted to the public domain, and administrators of this site will have full control over the said information.</FormHelperText>
                </Grid>
            </Grid>
            <FormControl fullWidth className={classes.margin} variant="outlined" >
                <Button type="submit" variant="outlined" id="createPlace">Create</Button>
            </FormControl>
        </form>
    );
};

export default PlaceCreationForm;

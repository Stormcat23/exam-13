let port = 8000;
if(process.env.REACT_APP_NODE_ENV === "test") {
    port = 8010;
}

export const labels = {
    0: "None",
    1: "Useless",
    2: "Poor",
    3: "Ok",
    4: "Good",
    5: "Excellent",
};

export const kindOfRating = ["qualityOfFood", "serviceQuality", "interior"];


export const apiURL = "http://localhost:" + port;
export const fbAppId = "2717404738523499";
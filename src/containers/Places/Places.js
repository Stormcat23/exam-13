import React, {useEffect} from "react";
import {Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import PlaceCard from "../../components/PlaceCard/PlaceCard";
import {fetchPlaces} from "../../store/actions/placesActions";

const Places = () => {
    const dispatch = useDispatch();
    const places = useSelector(state => state.places.places);

    useEffect(() => {
        dispatch(fetchPlaces());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container direction="row" justify="space-between" alignItems="center">
                <Grid item>
                    <p className="rainbow-animated">
                        Places
                    </p>
                </Grid>
            </Grid>
            <Grid item container direction="row" spacing={2}>
                {places && places.map(place => {
                    return <PlaceCard
                        key={place._id}
                        id={place._id}
                        title={place.title}
                        image={place.image}
                        datetime={place.datetime}
                    />
                })}
            </Grid>
        </Grid>
    );
};

export default Places;
import React from "react";
import {useDispatch} from "react-redux";
import {createPlace as onPlaceCreated} from "../../store/actions/placesActions";
import PlaceCreationForm from "../../components/PlaceCreationForm/PlaceCreationForm";

const NewPlace = () => {
    const dispatch = useDispatch();

    const createPlace = placeData => {
        dispatch(onPlaceCreated(placeData));
    };

    return (
        <>
            <h1>Add New Place</h1>
            <PlaceCreationForm
                onSubmit={createPlace}
            />
        </>
    );
};

export default NewPlace;